using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Clase que se activará al finalizar con éxito el Nivel3.
//Se encarga de mostrar la decoración, los mensajes y las opciones disponibles.
public class FinalNivel3 : MonoBehaviour
{    

    public GameObject decoracion;
    public GameObject dialogo;
    public GameObject opciones;
    

    GameObject botonInicio;
    GameObject botonNuevoIntento;    
    Text textoDialogos;

    void Start(){
        botonNuevoIntento = opciones.transform.GetChild(0).gameObject;
        botonInicio = opciones.transform.GetChild(3).gameObject;

        botonInicio.SetActive(false);
        botonNuevoIntento.SetActive(false);

        textoDialogos= dialogo.transform.GetChild(0).GetComponent<Text>();
    }
    private void OnCollisionEnter2D(Collision2D collision) {

        if (collision.transform.CompareTag("Player")){
           Nivel3.estado=Nivel3.Estados.Finalizada;
           decoracion.SetActive(true);         
           Invoke("MostrarLlave",1f);  
           JuegoControl.control.partida.llaves[2]=true;              
          
        }
        
    }

    void MostrarLlave(){
         decoracion.transform.GetChild(0).gameObject.SetActive(true);
         string textoDialogo="Bien hecho Xan, has superado la prueba de valor superando tus miedos y sobreviviendo a las almas del pasado. \n\n Aquí tienes la llave!";
         textoDialogos.text="";
         dialogo.SetActive(true);
         StartCoroutine(EscribirTexto(textoDialogos, textoDialogo,0.05f));
         botonInicio.SetActive(true);
    }

   
   //Mediante esta función simulamos un texto escrito caracter a caracter
    // con una velocidad determinada al llamarse como co-rutina
    // @param1 el componente Text que dónde escribiremos el texto
    // @param2 el mensaje
    // @param3 el tiempo que queremos esperar (x.xf) entre cada carácter
    IEnumerator EscribirTexto(Text texto,string mensaje, float velocidad)
    {
        foreach (char caracter in mensaje){
            texto.text = texto.text + caracter;
            yield return new WaitForSeconds(velocidad); 
        }    
    }


}


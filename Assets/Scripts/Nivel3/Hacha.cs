using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Clase para controlar el comportamiento del prefab de la hacha en lo relativo a movimiento
//y comportamiento respecto a los elementos con los que colisiona.
public class Hacha : MonoBehaviour
{
    public float fuerza=4f;
    public int angulo = 15;

    public SpriteRenderer sr;

    private int x=1;
    void Start()
    {
        if (sr.flipX){
            x=-1;
        } else {
            x=1;
        }
        GetComponent<Rigidbody2D>().AddForce(vectorFuerza(angulo,x,fuerza),ForceMode2D.Impulse);      
    }

   private void OnTriggerEnter2D(Collider2D collision) {

       if (collision.CompareTag("Enemigo")) {
            Destroy(gameObject);
        }

        if (collision.CompareTag("Suelo")) {
            Destroy(gameObject);
        }
                
    }

    void  OnDestroy() {
        JuegoControl.control.partida.lanzar=false;
        Nivel3.hachas--;
    }

    Vector2 vectorFuerza(int angulo, int direccion, float fuerza) {
         float posx = Mathf.Cos(angulo * Mathf.PI / 180)*fuerza;
         float posy = Mathf.Sin(angulo * Mathf.PI / 180)* fuerza;
         return new Vector2(posx*direccion,posy); 
    }
}

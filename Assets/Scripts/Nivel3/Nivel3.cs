using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

//Clase para controlar el Nivel3:
//-estado: Controla si el nivel a está pausado o aún no empezado (Espera), si estamos jugando (Iniciada) o si se hemos muerto / llegado al final (Finalizada)
//-hachas: Variable pública y estática que puede ser modificada desde otros scripts para el contador de hachas disponibles
//-contadorActivo: permite controlar si la partida está pausada o no
//-maxEnemigos: Establece el máximo de enemigos en la pantalla de juego.
//-enemigo: Prefab del enemigo que se irá instanciando en posiciones aleatorias.

public class Nivel3 : MonoBehaviour
{

    public GameObject dialogo;
    public GameObject opciones;
    public GameObject contenedorFade;
    public GameObject enemigo;
    public Text contador;
    public enum Estados { Pausada, Iniciada, Finalizada };
    public static Estados estado;
    public static int hachas;
    public int maxEnemigos;
    private bool contadorActivo;
    Animator fade;
    GameObject botonNuevoIntento;
    GameObject botonEmpezar;
    GameObject botonContinuar;
    GameObject botonInicio;


    private Text textoDialogos;


    void Start()
    {
        JuegoControl.control.Musica(false);
        contenedorFade.SetActive(true);
        fade = contenedorFade.GetComponent<Animator>();
        fade.SetBool("FadeOut", false);
        botonNuevoIntento = opciones.transform.GetChild(0).gameObject;
        botonEmpezar = opciones.transform.GetChild(1).gameObject;
        botonContinuar = opciones.transform.GetChild(2).gameObject;
        botonInicio = opciones.transform.GetChild(3).gameObject;

        string textoNivel = "No tienes miedo, pues entonces demuéstralo, y no permitas que las almas te toquen para llegar sano y salvo. Sólo tienes 15 hachas no las malgastes.\nMovimiento: ← flecha izq - flecha dcha →\nSalto: barra espaciadora\nLanzar hacha: tecla «x»\nPausa: tecla «p»";
        contadorActivo = false;
        estado = Estados.Pausada;
        dialogo.SetActive(true);

        textoDialogos = dialogo.transform.GetChild(0).GetComponent<Text>();
        StartCoroutine(EscribirTexto(textoDialogos, textoNivel, 0.02f));

        JuegoControl.control.partida.jugando = false;
        JuegoControl.control.partida.nivel = 3;
        Invoke("MostrarEmpezar", 3f);
        hachas = 15;
    }


    void FixedUpdate()
    {
        contador.text = hachas.ToString();

        if (estado == Estados.Iniciada && !contadorActivo)
        {
            dialogo.SetActive(false);
            EmpezarNivel();
        }

        if (estado == Estados.Finalizada)
        {
            FinalizarNivel();
        }

        if (ControlJugador.muerto && estado == Estados.Iniciada)
        {
            Invoke("MostrarMuerto", 1f);
            estado = Estados.Finalizada;
        }
        if (Input.GetKey("p"))
        {
            if (estado == Estados.Iniciada)
            {
                estado = Estados.Pausada;
                MostrarPausar();
            }
        }
    }
    void MostrarEmpezar()
    {
        botonEmpezar.SetActive(true);
    }
    public void EmpezarNivel()
    {
        JuegoControl.control.partida.jugando = true;
        ControlJugador.muerto = false;
        PrepararEnemigos();
        if (!contadorActivo)
        {
            contadorActivo = true;
        }
    }

    private void PrepararEnemigos()
    {
        for (int i = 0; i < maxEnemigos; i++)
        {
            //Instancia el enemigo en una posición aleatoria 
            float xAleatoria = Random.Range(-5.0f, 29.0f);
            Vector3 posicion = new Vector3(xAleatoria, 2, 0);
            Instantiate(enemigo, posicion, Quaternion.identity);
        }
    }

    public void FinalizarNivel()
    {
        JuegoControl.control.partida.jugando = false;
        contadorActivo = false;
    }

    void MostrarPausar()
    {
        JuegoControl.control.partida.jugando = false;
        opciones.SetActive(true);
        botonNuevoIntento.SetActive(true);
        botonEmpezar.SetActive(false);
        botonContinuar.SetActive(true);
        botonInicio.SetActive(true);
    }

    void MostrarMuerto()
    {
        string textoFinalTiempo = "Lo siento Xan pero no has sobrevivido a las almas del pasado, y no mereces la llave de esta prueba. \n\nPuedes volver a intentarlo, pero ten más cuidado esta vez.";
        textoDialogos.text = "";
        dialogo.SetActive(true);
        StartCoroutine(EscribirTexto(textoDialogos, textoFinalTiempo, 0.05f));
        Invoke("MostrarOpciones", 1f);
    }

    void MostrarOpciones()
    {
        botonInicio.SetActive(true);
        botonNuevoIntento.SetActive(true);
    }

    //Mediante esta función simulamos un texto escrito caracter a caracter
    // con una velocidad determinada al llamarse como co-rutina
    // @param1 el componente Text que dónde escribiremos el texto
    // @param2 el mensaje
    // @param3 el tiempo que queremos esperar (x.xf) entre cada carácter
    IEnumerator EscribirTexto(Text texto, string mensaje, float velocidad)
    {
        foreach (char caracter in mensaje)
        {
            texto.text = texto.text + caracter;
            yield return new WaitForSeconds(velocidad);
        }
    }


}

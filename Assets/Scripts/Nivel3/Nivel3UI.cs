using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//Clase con los métodos empleados por los botones de la UI en la escena Nivel3
public class Nivel3UI : MonoBehaviour
{
    public GameObject opciones;
    public void EmpezarNivel(){
      Nivel3.estado=Nivel3.Estados.Iniciada;  
      gameObject.SetActive(false);
    }

    public void NuevoIntento(){
      SceneManager.LoadScene("Nivel3");
    }

    public void VolverInicio(){
      SceneManager.LoadScene("Nivel0");
    }

    public void ReanudarJuego(){ 
      Nivel3.estado=Nivel3.Estados.Iniciada; 
      JuegoControl.control.partida.jugando=true; 
      opciones.transform.GetChild(0).gameObject.SetActive(false);
      opciones.transform.GetChild(2).gameObject.SetActive(false);
      opciones.transform.GetChild(3).gameObject.SetActive(false);
     
    }
   
}

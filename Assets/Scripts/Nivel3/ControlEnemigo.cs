using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase para el control del enemigo (prefab):
//- Controla la información del enemigo (velocidad y dirección) 
//- Hacemos que se mueva y cambie de dirección si encuentra obstáculo en terreno,
//  choca contra otro enemigo o de manera aleatoria mediante un contador propio.
//- Hacemos que emita sonido si choca jugador y provoque su muerte.
//- Hacemos que se destruya si es alcanzado por un arma.

public class ControlEnemigo : MonoBehaviour
{
    public float velocidad = 0.5f;
    public bool direccion = true;  // lo tomamos como referencia hacia la izquierda
    public GameObject cuerpo;
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private BoxCollider2D bc;

    private bool contadorEnemigo;
    private int tiempo;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = cuerpo.GetComponent<SpriteRenderer>();
        bc = GetComponent<BoxCollider2D>();
    }

    //Asignamos un tiempo aleatorio de cambio de dirección entre 5 y 10 segundos.
    void Start()
    {

        tiempo = Random.Range(5, 10);
        if (!contadorEnemigo)
        {
            contadorEnemigo = true;
            Invoke("Contar", 1f);
        }

    }


    //Comprobamos las colisiones entorno, enemigos y jugador
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Suelo"))
        {
            CambiarDireccion();
        }

        if (collision.transform.CompareTag("Player"))
        {
            ControlJugador.muerto = true;
            AudioSource audio = gameObject.GetComponent<AudioSource>();
            audio.PlayOneShot(audio.clip);
        }

        if (collision.transform.CompareTag("Enemigo"))
        {
            CambiarDireccion();
        }

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("Arma"))
        {
            sr.enabled = false;
            bc.enabled = false;
            rb.velocity = new Vector2(0, 0);
            cuerpo.transform.GetChild(0).gameObject.SetActive(true);
            //Hacemos sonido destruir
            AudioSource audio = cuerpo.transform.GetChild(0).gameObject.GetComponent<AudioSource>();
            audio.PlayOneShot(audio.clip);
            //Destruimos el objeto enemigo
            Destroy(gameObject, 0.3f);
        }

    }

    void FixedUpdate()
    {
        if (JuegoControl.control.partida.jugando)
        {
            if (direccion)
            {
                sr.flipX = true;
                rb.velocity = new Vector2(-velocidad, rb.velocity.y);
            }
            else
            {
                sr.flipX = false;
                rb.velocity = new Vector2(+velocidad, rb.velocity.y);
            }

        }
        else
        {
            rb.velocity = new Vector2(0, 0);
        }
    }

    private void CambiarDireccion()
    {
        direccion = !direccion;

    }


    void Contar()
    {
        if (contadorEnemigo)
        {
            tiempo--;
            if (tiempo >= 0)
            {
                Invoke("Contar", 1f);
            }
            else
            {
                CambiarDireccion(); //cambiamos dirección
                tiempo = Random.Range(5, 10); //asignamos otro tiempo aleatorio
                Invoke("Contar", 1f);
            }
        }
    }
}


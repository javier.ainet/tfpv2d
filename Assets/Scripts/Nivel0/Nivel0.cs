using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Clase para controlar el Nivel0:
//-estado: Controla si el jugador está moviéndose (Espera), si el Mago habla (Hablar) o si podemos elegir nivel (Jugar)
//-llaves: Grupo de objetos que muestra la UI para elegir nivel y llaves conseguidas o no.
//-dialogo: Grupo de objetos donde se mostrará el texto del diálogo del Mago

public class Nivel0 : MonoBehaviour
{
    public GameObject dialogo;
    public GameObject mago;
    public GameObject jugador;
    public GameObject contenedorFade;
    public GameObject llaves;
    
    public enum Estados {Espera, Hablar, Jugar};
    public static Estados estado;

    Text textoDialogos;
    Animator fade;

    string[] dialogos= new string[4];

   
    void Start()
    {
        JuegoControl.control.Musica(true);
        contenedorFade.SetActive(true);
        fade = contenedorFade.GetComponent<Animator>();
        fade.SetBool("FadeOut",false); 
        dialogos[0]= "Estimado Xan recuerda que para conseguir las llaves debes superar cada una de las pruebas.\nVelocidad, Puntería y Valentía son las habilidades que debes demostrar para superarlas.\nConfía siempre en tí y a por ellas.";
        dialogos[1]= "Ya tienes tu primera llave, ahora es el momento de buscar la siguiente.\n¿Qué prueba estas dispuesto a superar ahora?\nVamos allá!";
        dialogos[2]= "Lo estás haciendo genial, Ya sólo falta una prueba y la última llave será tuya.\nUn último esfuerzo y podrás pertenecer al círculo D'Arzúa";
        dialogos[3]= "Enhorabueba Xan has demostrado que eres rápido, certero y valiente, ya eres uno de los nuestros y podrás aprender de nuestro clan todo necesitas para ser un auténtico lider.\n\n POR D'ARZÚA !!!";
        llaves.SetActive(false);
        estado=Estados.Espera; 
        textoDialogos= dialogo.transform.GetChild(0).GetComponent<Text>();
    }

    void FixedUpdate()    
    {     
        if (estado==Estados.Hablar) {
            dialogo.SetActive(true);
            int numLlaves= JuegoControl.control.partida.ContarLlaves();
            StartCoroutine(EscribirTexto(textoDialogos,dialogos[numLlaves],0.05f));
            estado=Estados.Espera;             
        }

         if (estado==Estados.Jugar) {
            Invoke("MostrarLlaves",2f);
            int numLlaves= JuegoControl.control.partida.ContarLlaves();
            //Si tenemos las 3 llaves nos lleva a la pantalla final
            if (numLlaves==3) {
                fade.SetBool("FadeOut",true); 
                Invoke("Finalizar",4f);
            }
         }
     
    }


    //Mediante esta función simulamos un texto escrito caracter a caracter
    // con una velocidad determinada al llamarse como co-rutina
    // @param1 el componente Text que dónde escribiremos el texto
    // @param2 el mensaje
    // @param3 el tiempo que queremos esperar (x.xf) entre cada carácter
    IEnumerator EscribirTexto(Text texto,string mensaje, float velocidad)
    {
        foreach (char caracter in mensaje){
            texto.text = texto.text + caracter;
            yield return new WaitForSeconds(velocidad); 
        }    
        estado=Estados.Jugar;
    }


    //Método para mostrar las llaves y su estado.
    void MostrarLlaves(){
        dialogo.SetActive(false);
        llaves.SetActive(true);
        if (!JuegoControl.control.partida.llaves[0]){
            llaves.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.SetActive(false);
        } else {
            llaves.transform.GetChild(0).gameObject.transform.GetChild(1).gameObject.SetActive(false);
        }
        if (!JuegoControl.control.partida.llaves[1]){
            llaves.transform.GetChild(1).gameObject.transform.GetChild(0).gameObject.SetActive(false);
        } else {
            llaves.transform.GetChild(1).gameObject.transform.GetChild(1).gameObject.SetActive(false);
        }
        if (!JuegoControl.control.partida.llaves[2]){
            llaves.transform.GetChild(2).gameObject.transform.GetChild(0).gameObject.SetActive(false);
        } else {
            llaves.transform.GetChild(2).gameObject.transform.GetChild(1).gameObject.SetActive(false);
        }
     }   

     void Finalizar(){
         SceneManager.LoadScene("Final");
     }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase para la escena del nivel0 donde el jugador 
//se desplaza hasta llegar junto al Mago y escuchar sus explicaciones 


public class JugadorNivel0 : MonoBehaviour
{
    public Animator anim;

    Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim.SetBool("Correr", true);
        rb.velocity= new Vector2(2.0f,rb.velocity.y);        
    }

 
    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.transform.CompareTag("PuntoEspera")) {
            anim.SetBool("Correr", false);    
            rb.velocity= new Vector2(0,rb.velocity.y); 
            Nivel0.estado=Nivel0.Estados.Hablar;       
        }        
    } 
}

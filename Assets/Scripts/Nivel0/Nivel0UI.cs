using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//Clase con los métodos empleados por los botones de la UI en la escena Nivel0

public class Nivel0UI : MonoBehaviour
{
    public void JugarNivel1(){
        JuegoControl.control.partida.nivel=1;
        SceneManager.LoadScene("Nivel1");
    }

    public void JugarNivel2(){
        JuegoControl.control.partida.nivel=2;
        SceneManager.LoadScene("Nivel2");
    }

    public void JugarNivel3(){
        JuegoControl.control.partida.nivel=3;
        SceneManager.LoadScene("Nivel3");
    }

    public void MenuPrincipal(){
        SceneManager.LoadScene("Menu");
    }

    public void Omitir(){
        Nivel0.estado=Nivel0.Estados.Jugar;
    }
}


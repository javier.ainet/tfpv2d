using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase para controlar el movimiento automatico jugador en la escena final 
public class JugadorFinal : MonoBehaviour
{
    public Animator anim;
    public GameObject gameOver;

    Rigidbody2D rb;

    //Se inicia movimiento del sprite del jugador al comienzo
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim.SetTrigger("Andar");
        rb.velocity= new Vector2(-0.6f,rb.velocity.y);        
    }

    //Al llegar a la puerta se para la animación y movimiento jugador
    //y muestra el texto de Game Over.
    private void OnTriggerEnter2D(Collider2D salida) {
        if (salida.transform.CompareTag("PuntoEspera")){
                   gameOver.SetActive(true);
                   anim.SetTrigger("Parar");
                   Final.salir=true;                   
                   rb.velocity= new Vector2(0f,0f);
        }

    }

}

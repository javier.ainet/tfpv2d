using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase para la escena del menú principal que gestiona el Fade In-Out 
public class Menu : MonoBehaviour
{
    public GameObject contenedorFade;
    public static bool salir;
    Animator fade;
    
    void Start()
    {
        contenedorFade.SetActive(true);
        fade = contenedorFade.GetComponent<Animator>();
        fade.SetBool("FadeOut",false); 
        salir=false;
    }

    void Update()
    {
        if (salir) {
          fade.SetBool("FadeOut",true); 
        }

    }
}

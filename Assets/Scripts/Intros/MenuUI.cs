using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Clase que contiene los métodos que se asocian a los botones de
//la UI del menú principal. Algunas de las acciones están planteadas
//como corrutinas para dar tiempo a la animación del efecto Fade.
public class MenuUI : MonoBehaviour
{

    public void Salir()
    {
        Menu.salir = true;
        StartCoroutine(SalirAplicacion(3f));
    }

    public void NuevaPartida()
    {
        JuegoControl.control.partida.IniciarPartida();
        SceneManager.LoadScene("Nivel0");
    }

    public void ContinuarPartida()
    {
        if (JuegoControl.control.partida.PartidaIniciada())
        {
            SceneManager.LoadScene("Nivel0");
        }
    }

    public void MenuPrincipal()
    {
        SceneManager.LoadScene("Menu");
    }


    public void VerIntro()
    {
        Menu.salir = true;
        StartCoroutine(CargarEscena("Intro", 3f));
    }

    public void VerInstrucciones()
    {
        Menu.salir = true;
        StartCoroutine(CargarEscena("Instrucciones", 3f));
    }

    public void VerCreditos()
    {
        SceneManager.LoadScene("Creditos");
    }
    public void OmitirIntro()
    {
        Intro.salir = true;
    }

    private IEnumerator CargarEscena(string escena, float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        SceneManager.LoadScene(escena);
    }

    private IEnumerator SalirAplicacion(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        Application.Quit();
    }

}

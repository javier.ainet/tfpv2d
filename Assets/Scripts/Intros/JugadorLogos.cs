using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase para la escena de los logos donde el jugador 
//se desplaza hasta llegar a un punto de salida 
//provocando un cambio en la escena logo para cargar la intro.

public class JugadorLogos : MonoBehaviour
{
    public Animator anim;
    Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim.SetTrigger("Andar");
        rb.velocity= new Vector2(0.8f,rb.velocity.y);        
    }

    private void OnTriggerEnter2D(Collider2D salida) {
        if (salida.transform.CompareTag("PuntoEspera")){
             Logo.salir=true;
        }
    }
}
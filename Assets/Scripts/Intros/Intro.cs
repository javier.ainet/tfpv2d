using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Clase para la escena de la intro que gestiona el Fade In-Out 
//la carga del texto de la intro desde un archivo de texto.

public class Intro : MonoBehaviour
{
    public GameObject contenedorFade;
    public GameObject dialogo;

    public GameObject botonOmitir;  
    public static bool salir;

    Text textoDialogos;
    Animator fade;



    void Start() {
        contenedorFade.SetActive(true);
        fade = contenedorFade.GetComponent<Animator>();
        salir=false;
        TextAsset textFile = (TextAsset) Resources.Load ("intro", typeof (TextAsset));
        fade.SetBool("FadeOut",false);   
        dialogo.SetActive(true);      
        textoDialogos= dialogo.transform.GetChild(0).transform.GetChild(0).GetComponent<Text>();
        StartCoroutine(EscribirTexto(textoDialogos, textFile.ToString(),0.05f));
        botonOmitir.SetActive(true);
        Invoke("MostrarOmitir",3f);        
    }

    void FixedUpdate() {
        if (salir)      {
           fade.SetBool("FadeOut",true); 
           Invoke("MostrarMenu",5f);
        }
    }

   void MostrarOmitir(){
        botonOmitir.SetActive(true);
    }
    
   void MostrarMenu(){
        SceneManager.LoadScene("Menu");
    }

   //Mediante esta función simulamos un texto escrito caracter a caracter
    // con una velocidad determinada al llamarse como co-rutina
    // @param1 el componente Text que dónde escribiremos el texto
    // @param2 el mensaje
    // @param3 el tiempo que queremos esperar (x.xf) entre cada carácter
    IEnumerator EscribirTexto(Text texto,string mensaje, float velocidad)
    {
        foreach (char caracter in mensaje){
            texto.text = texto.text + caracter;
            yield return new WaitForSeconds(velocidad); 
        }  
        salir=true;
    }
}

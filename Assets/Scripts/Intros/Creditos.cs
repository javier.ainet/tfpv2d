using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Clase para la escena de los créditos que gestiona el Fade In-Out 
//y la carga de los créditos de un archivo de texto.

public class Creditos : MonoBehaviour
{

    public Text texto;
    public GameObject contenedorFade;

    Animator fade;
    void Start()
    {
        contenedorFade.SetActive(true);
        fade = contenedorFade.GetComponent<Animator>();
        fade.SetBool("FadeOut",false); 
        TextAsset textFile = (TextAsset) Resources.Load ("creditos", typeof (TextAsset));
        texto.text = textFile.ToString(); 
    }


}

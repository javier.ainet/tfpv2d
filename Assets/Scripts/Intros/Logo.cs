using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Clase para la escena inicial con los Logos que gestiona el Fade In-Out 
//y activa la salida hacia la escena Intro
public class Logo : MonoBehaviour
{
    public GameObject contenedorFade;
    public static bool salir;
    Animator fade;

    void Start() {
        contenedorFade.SetActive(true);
        fade = contenedorFade.GetComponent<Animator>();
        fade.SetBool("FadeOut",false);        
        salir=false;
    }

    void FixedUpdate() {
        if (salir)      {
           fade.SetBool("FadeOut",true); 
           Invoke("MostrarIntro",5f);
        }
    }

    void MostrarIntro(){
         SceneManager.LoadScene("Intro");
    }

}

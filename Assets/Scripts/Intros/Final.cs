using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Final : MonoBehaviour
{
  public GameObject contenedorFade;
    public static bool salir;

    Animator fade;

   //Clase para la escena final (Game Over) del juego que gestiona el Fade In-Out 
   //y limpiar los datos de la partida antes de volver al menú principal.

    void Start() {
        contenedorFade.SetActive(true);
        fade = contenedorFade.GetComponent<Animator>();
        fade.SetBool("FadeOut",false); 
        salir=false;
    }

    void FixedUpdate() {
        if (salir)      {
           fade.SetBool("FadeOut",true); 
           Invoke("MostrarMenu",4f);
        }
    }
 
    
   void MostrarMenu(){
       JuegoControl.control.partida.LimpiarPartida();
       SceneManager.LoadScene("Menu");
    }

 
}

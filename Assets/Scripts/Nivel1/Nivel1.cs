using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Clase para controlar el Nivel1:
//-estado: Controla si el nivel a está pausado o aún no empezado (Espera), si estamos jugando (Iniciada) o si se ha terminado el nivel/tiempo (Finalizada)
//-tiempo: Variable pública y estática que puede ser modificada desde otros scripts para el contador del tiempo
//-calabazas: Variable pública y estática que puede ser modificada desde otros scripts para el contador de calabazas
//-contadorActivo: permite controlar si se activa o no el contador de tiempo.



public class Nivel1 : MonoBehaviour
{

    public static int tiempo;
    public static int calabazas;
    public Text marcador;
    public Text contador;
    public GameObject dialogo;
    public GameObject opciones;
    public GameObject contenedorFade;
    Animator fade;
    GameObject botonNuevoIntento;
    GameObject botonEmpezar;
    GameObject botonContinuar;
    GameObject botonInicio;
    GameObject botonPausar;

    public enum Estados { Pausada, Iniciada, Finalizada };
    public static Estados estado;

    private bool contadorActivo;
    Text textoDialogos;
    Vector2 posicionInicial;


    void Start()
    {
        JuegoControl.control.Musica(false);
        contenedorFade.SetActive(true);
        fade = contenedorFade.GetComponent<Animator>();
        fade.SetBool("FadeOut", false);
        botonNuevoIntento = opciones.transform.GetChild(0).gameObject;
        botonEmpezar = opciones.transform.GetChild(1).gameObject;
        botonContinuar = opciones.transform.GetChild(2).gameObject;
        botonInicio = opciones.transform.GetChild(3).gameObject;
        string textoNivel = "Para conseguir la llave de esta prueba debes recoger las 25 calabazas y llegar a la estatua antes de terminar el tiempo.\nBusca bien y demuestra que eres rápido.\nMovimiento: ← flecha izq - flecha dcha →\nSalto: barra espaciadora\nPausa: tecla «p»";
        calabazas = 0;
        tiempo = 90;
        contadorActivo = false;
        estado = Estados.Pausada;

        dialogo.SetActive(true);
        textoDialogos = dialogo.transform.GetChild(0).GetComponent<Text>();
        StartCoroutine(EscribirTexto(textoDialogos, textoNivel, 0.05f));

        JuegoControl.control.partida.jugando = false;
        JuegoControl.control.partida.nivel = 1;
        Invoke("MostrarEmpezar", 4f);
    }

    void FixedUpdate()
    {
        contador.text = tiempo.ToString();
        marcador.text = calabazas.ToString();

        if (estado == Estados.Iniciada && !contadorActivo)
        {
            dialogo.SetActive(false);
            EmpezarNivel();
        }

        if (estado == Estados.Finalizada)
        {
            FinalizarNivel();
        }

        if (tiempo <= 0 && estado == Estados.Iniciada)
        {
            Invoke("MostrarGameOverTiempo", 1f);
            estado = Estados.Finalizada;
        }

        if (estado == Estados.Pausada)
        {
            contadorActivo = false;
        }

        if (Input.GetKey("p"))
        {
            if (estado == Estados.Iniciada)
            {
                estado = Estados.Pausada;
                MostrarPausar();
            }
        }
    }

    public void EmpezarNivel()
    {
        JuegoControl.control.partida.jugando = true;
        if (!contadorActivo)
        {
            contadorActivo = true;
            Invoke("Contar", 1f);
        }
    }


    public void FinalizarNivel()
    {
        JuegoControl.control.partida.jugando = false;
        contadorActivo = false;
    }

    //Método que nos permite contar el tiempo descontando una unidad cada segundo.
    void Contar()
    {
        if (contadorActivo)
        {
            tiempo--;
            if (tiempo > 0)
            {
                Invoke("Contar", 1f);
            }
            else
            {
                tiempo = 0;
            }
        }
    }


    void MostrarEmpezar()
    {
        opciones.SetActive(true);
        botonNuevoIntento.SetActive(false);
        botonEmpezar.SetActive(true);
        botonContinuar.SetActive(false);
        botonInicio.SetActive(false);
    }

    void MostrarPausar()
    {
        JuegoControl.control.partida.jugando = false;
        opciones.SetActive(true);
        botonNuevoIntento.SetActive(true);
        botonEmpezar.SetActive(false);
        botonContinuar.SetActive(true);
        botonInicio.SetActive(true);
    }

    void MostrarOpciones()
    {
        botonInicio.SetActive(true);
        botonNuevoIntento.SetActive(true);
    }

    void MostrarGameOverTiempo()
    {
        string textoFinalTiempo = "Lo siento Xan, pero el tiempo se ha agotado y no mereces la llave de esta prueba. \n Vuelve a intentarlo más tarde.";
        textoDialogos.text = "";
        dialogo.SetActive(true);
        StartCoroutine(EscribirTexto(textoDialogos, textoFinalTiempo, 0.05f));
        Invoke("MostrarOpciones", 6f);
    }


    //Mediante esta función simulamos un texto escrito caracter a caracter
    // con una velocidad determinada al llamarse como co-rutina
    // @param1 el componente Text que dónde escribiremos el texto
    // @param2 el mensaje
    // @param3 el tiempo que queremos esperar (x.xf) entre cada carácter
    IEnumerator EscribirTexto(Text texto, string mensaje, float velocidad)
    {
        foreach (char caracter in mensaje)
        {
            texto.text = texto.text + caracter;
            yield return new WaitForSeconds(velocidad);
        }
    }

}

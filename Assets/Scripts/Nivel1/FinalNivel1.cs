using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//Clase que controla el proceso de llegar al final del Nivel1 junto a la estatua.
//Se encarga de mostrar la decoración, los mensajes y las opciones en
//función del resultado de la prueba.
public class FinalNivel1 : MonoBehaviour
{    

    public GameObject decoracion;
    public GameObject dialogo;
    public GameObject opciones;

    GameObject botonInicio;
    GameObject botonNuevoIntento;  

    Text textoDialogos;

    void Start(){
        botonNuevoIntento = opciones.transform.GetChild(0).gameObject;
        botonInicio = opciones.transform.GetChild(3).gameObject;
       
        botonInicio.SetActive(false);
        botonNuevoIntento.SetActive(false);

        textoDialogos= dialogo.transform.GetChild(0).GetComponent<Text>();
    }
    private void OnCollisionEnter2D(Collision2D collision) {

        if (collision.transform.CompareTag("Player")){
           Nivel1.estado=Nivel1.Estados.Finalizada;
           decoracion.SetActive(true);
           
           if (Nivel1.calabazas==25 && Nivel1.tiempo>0){
                Invoke("MostrarLlave",1f);
                JuegoControl.control.partida.llaves[0]=true;
           }  

            if (Nivel1.tiempo<=0) {
                Invoke("MostrarGameOverTiempo",1f);
            }

            if (Nivel1.calabazas<25 && Nivel1.tiempo>0) {
                Invoke("MostrarGameOverCantidad",1f);
            }
        }
        
    }

    void MostrarLlave(){
         decoracion.transform.GetChild(0).gameObject.SetActive(true);
         string textoDialogo="Bien hecho Xan, has superado la prueba de velocidad. \n Aquí tienes la llave.";
         textoDialogos.text="";
         dialogo.SetActive(true);
         StartCoroutine(EscribirTexto(textoDialogos, textoDialogo,0.05f));
         botonInicio.SetActive(true);
    }

    void MostrarGameOverTiempo(){
         decoracion.transform.GetChild(1).gameObject.SetActive(false);
         string textoDialogo="Lo siento Xan, pero el tiempo se ha agotado y no mereces la llave de esta prueba. \n Vuelve a intentarlo más tarde.";
         textoDialogos.text="";
         dialogo.SetActive(true);
         StartCoroutine(EscribirTexto(textoDialogos, textoDialogo,0.05f));
         botonInicio.SetActive(true);
         botonNuevoIntento.SetActive(true);

   
    }

    void MostrarGameOverCantidad(){
         decoracion.transform.GetChild(1).gameObject.SetActive(false); 
         string textoDialogo="Has sido rápido Xan, pero no has superado la prueba aquí faltan calabazas.  \n Vuelve a intentarlo más tarde..";
         textoDialogos.text="";
         dialogo.SetActive(true);
         StartCoroutine(EscribirTexto(textoDialogos, textoDialogo,0.05f));
         botonInicio.SetActive(true);  
         botonNuevoIntento.SetActive(true);     
    }


   //Mediante esta función simulamos un texto escrito caracter a caracter
    // con una velocidad determinada al llamarse como co-rutina
    // @param1 el componente Text que dónde escribiremos el texto
    // @param2 el mensaje
    // @param3 el tiempo que queremos esperar (x.xf) entre cada carácter
    IEnumerator EscribirTexto(Text texto,string mensaje, float velocidad)
    {
        foreach (char caracter in mensaje){
            texto.text = texto.text + caracter;
            yield return new WaitForSeconds(velocidad); 
        }    
    }


}


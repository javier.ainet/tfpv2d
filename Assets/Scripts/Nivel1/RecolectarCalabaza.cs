using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Esta clase se aplica a los objetos premio del juego (En este caso un prefab con forma de calabaza)
//El comportamiento es si el jugador pasa por el objeto (collider en trigger) entonces este desaparece
//visualemente, genera sonido y muestra animación de recolectar, se destruye y aumenta el contador.
public class RecolectarCalabaza : MonoBehaviour
{
    public int puntuacion=1;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            AudioSource audio = gameObject.GetComponent<AudioSource>();
            audio.PlayOneShot(audio.clip);             
            GetComponent<SpriteRenderer>().enabled = false;
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            Destroy(gameObject, 0.5f);            
        }        
    }

     //Al destruir el objeto sumamos aumentamos 1 calabaza
    void OnDestroy() {
        Nivel1.calabazas++;
    }

}

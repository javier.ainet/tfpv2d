using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Clase con los métodos empleados por los botones de la UI en la escena Nivel1

public class Nivel1UI:MonoBehaviour
{
    public GameObject opciones;

    public void EmpezarNivel(){
      Nivel1.estado=Nivel1.Estados.Iniciada;  
      gameObject.SetActive(false);
    }

    public void NuevoIntento(){
      SceneManager.LoadScene("Nivel1");
    }

    public void VolverInicio(){
      SceneManager.LoadScene("Nivel0");
    }



    public void ReanudarJuego(){ 
      Nivel1.estado=Nivel1.Estados.Iniciada; 
      JuegoControl.control.partida.jugando=true; 
      opciones.transform.GetChild(0).gameObject.SetActive(false);
      opciones.transform.GetChild(2).gameObject.SetActive(false);
      opciones.transform.GetChild(3).gameObject.SetActive(false);
     
    }
   
   
}

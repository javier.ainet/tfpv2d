using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Clase asociada a un objeto del jugador que contiene un collider en formato trigger
//para conocer si el jugador está en el suelo y poder saltar.
public class ControlSuelo : MonoBehaviour
{
    public static bool suelo;
    public static bool muerto;

    void Start() {
        
    }

    private void OnTriggerEnter2D(Collider2D collision){
       if (collision.transform.CompareTag("Suelo")){
            suelo=true;
       }  

       if (collision.transform.CompareTag("Enemigo")){
            muerto=true;
       }  
    }

    private void OnTriggerExit2D(Collider2D collision){

        if (collision.transform.CompareTag("Suelo")){
            suelo=false;
        }  
      
    }
    

}

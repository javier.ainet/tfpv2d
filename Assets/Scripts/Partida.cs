using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase para crear una partida con todos los datos necesarios y métodos para conocer y cambiar estados o datos.
public class Partida 
{
    public enum Estados {Espera, Iniciada, Finalizada}

    public bool[] llaves = new bool[3];

    public Estados estado; //Estado de la partida

    public int nivel; //nivel que estamos jugando

    public bool jugando; //controlar si ha empezado el nivel

    public bool lanzar; //controlar si se lanza algo

    public Partida(){
        this.estado=Estados.Espera;
        this.jugando=false;
        this.lanzar=false;
        llaves= new bool[3];
    }

    public int ContarLlaves(){
        int contador=0;
        foreach (bool llave in llaves){
            if (llave) contador++;
        }
        return contador;
    } 

    public bool PartidaIniciada(){
        if (estado==Estados.Iniciada){
            return true;
        } else  {
            return false;
        }
    }

    public void IniciarPartida(){
        llaves= new bool[3];
        estado=Estados.Iniciada;
    }

    public void LimpiarPartida(){
        this.estado=Estados.Espera;
        this.jugando=false;
        this.lanzar=false;
        this.llaves= new bool[3];
    }
    
}

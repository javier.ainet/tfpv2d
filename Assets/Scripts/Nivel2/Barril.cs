using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Clase para controlar el comportamiento del prefab del barril cuando
//es alcanzado por una bala, tanto en animación como en audio.
public class Barril : MonoBehaviour
{
    Animator anim;
    void Start()
    {
        anim=GetComponent<Animator>();
        anim.SetBool("Destruir",false);
    }

    private void OnTriggerEnter2D(Collider2D collision) {  

       if (collision.CompareTag("Arma")) {
             anim.SetBool("Destruir",true);          
             //Hacemos sonido de recolectar puntos
             AudioSource audio= GetComponent<AudioSource>();
             audio.PlayOneShot(audio.clip);   
             //Destruimos el objeto enemigo
             Destroy(gameObject,1f); 
        }    
        
    }

    private void OnDestroy() {
        Nivel2.barriles--;
    }
}

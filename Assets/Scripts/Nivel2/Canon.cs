using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Clase para controlar el comportamiento del cañón y los elementos con los que interactúa
// -lr : Permite mostrar una línea de disparo para ver la dirección y fuerza
// -bala: Es el prefab que se creará al disparar
// -animJugador: Permite animar al jugador para desplazar lo al lado contrario del que apunta el cañón.
// -fuerza: Almacena la fuerza que le vamos a aplicar a la bala
// -angulo: nos permite actualizar la posición del cañón y la dirección de salida de la bala.
// -disparo: variable pública estática que permite conocer su valor en otros scripts y no permitir 2 disparos
//           seguidos por ejemplo. 

public class Canon : MonoBehaviour
{
    public LineRenderer lr;
    public GameObject bala; //prefab de la bala
    public Animator animJugador;
    float fuerza;
    int angulo;
    float multiplicador=5f;
    public static bool disparo;
    
    void Start()
    {
      lr.SetPosition(0,transform.position);
      lr.SetPosition(1,transform.position);        
      disparo=false;
    }

   void Update(){
        if (JuegoControl.control.partida.jugando){
            if (Input.GetMouseButtonDown(0) && !disparo){                       
                disparo=true;
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                GameObject balaInstancia = Instantiate(bala,transform.position,transform.rotation);       
                balaInstancia.GetComponent<Rigidbody2D>().velocity=transform.up.normalized * (fuerza + multiplicador);            
                AudioSource audio= gameObject.GetComponent<AudioSource>(); 
                audio.PlayOneShot(audio.clip);  
                Nivel2.balas--;
            }
        }
   }
    void FixedUpdate()
    {
        if (JuegoControl.control.partida.jugando){
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePos - transform.position);
            Vector3 lineaPos=new Vector3(mousePos.x,mousePos.y,0f);
            lr.SetPosition(1,lineaPos);
            fuerza = Distancia (lr.GetPosition(0), lr.GetPosition(1));   
            angulo = Angulo (lr.GetPosition(1), lr.GetPosition(0));
            if (angulo>90 & animJugador.GetBool("Izquierda")) {
                animJugador.SetBool("Izquierda",false);  
                animJugador.SetTrigger("Cambiar");
            }
            if (angulo<90 & !animJugador.GetBool("Izquierda")) {
                animJugador.SetBool("Izquierda",true);
                animJugador.SetTrigger("Cambiar");
            }
        }    

        if (!JuegoControl.control.partida.jugando) {
            animJugador.SetBool("Izquierda",true);
            animJugador.SetTrigger("Cambiar");
        }

    }

    //Método auxiliar para calcular distancia entre 2 puntos
    float  Distancia (Vector2 P1, Vector2 P2) {
        return  Vector2.Distance(P1,P2);
    }

    //Método auxiliar para calcular angulo entre 2 puntos
    int Angulo (Vector3 P1, Vector3 P2){
        Vector3 vector= P1-P2;
        vector.Normalize();
        float angulo = Mathf.Atan2(vector.y,vector.x)*Mathf.Rad2Deg;
        return (int) angulo;
    }
}

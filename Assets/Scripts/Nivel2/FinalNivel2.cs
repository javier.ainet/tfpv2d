using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//Clase que se activará al finalizar con éxito el Nivel2.
//Se encarga de mostrar la decoración, los mensajes y las opciones disponibles.

public class FinalNivel2 : MonoBehaviour
{

    public GameObject dialogo;
    public GameObject opciones;
    Text textoDialogos;

    GameObject botonInicio;
    GameObject fuego;
    GameObject llave;

    void Start(){     
        botonInicio = opciones.transform.GetChild(3).gameObject;
        fuego = transform.GetChild(1).gameObject.transform.GetChild(0).gameObject;
        llave = transform.GetChild(0).gameObject;     
        textoDialogos= dialogo.transform.GetChild(0).GetComponent<Text>();        
        Invoke("MostrarLlave",1f);  
        JuegoControl.control.partida.llaves[1]=true; 
    }
   
    void MostrarLlave(){
         fuego.SetActive(true);
         llave.SetActive(true);
         string textoDialogo="Buena puntería, has conseguido no malgastar las balas y destruir 10 barriles.\n\nAquí tienes la llave de esta prueba!";
         textoDialogos.text="";
         dialogo.SetActive(true);
         StartCoroutine(EscribirTexto(textoDialogos, textoDialogo,0.05f));
         botonInicio.SetActive(true);
    }

   
   //Mediante esta función simulamos un texto escrito caracter a caracter
    // con una velocidad determinada al llamarse como co-rutina
    // @param1 el componente Text que dónde escribiremos el texto
    // @param2 el mensaje
    // @param3 el tiempo que queremos esperar (x.xf) entre cada carácter
    IEnumerator EscribirTexto(Text texto,string mensaje, float velocidad)
    {
        foreach (char caracter in mensaje){
            texto.text = texto.text + caracter;
            yield return new WaitForSeconds(velocidad); 
        }    
    }

}

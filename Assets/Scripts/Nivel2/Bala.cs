using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase para controlar el comportamiento del prefab de la bala en lo relativo a movimiento
//y comportamiento respecto a los elementos con los que colisiona.
public class Bala : MonoBehaviour
{
    Rigidbody2D rb;
    SpriteRenderer sr;
    GameObject exp;
    

    private bool activo; 
    void Start()
    {
        rb=gameObject.GetComponent<Rigidbody2D>();
        sr=gameObject.GetComponent<SpriteRenderer>();
        activo=true;
    }

    // Update is called once per frame
    void Update()
    {
         //Mientras se mueve actualizamos el ángulo de rotación del proyectil
       if (rb.velocity != Vector2.zero) {
           Vector2 dir = rb.velocity;
           float angulo = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg -90;
           transform.rotation= Quaternion.AngleAxis(angulo,Vector3.forward);
           //Si el proyectil está por debajo de este valor (fuera del escenario explota)
           
       }  

       if (rb.position.y<=-15) {  
               //Antes de explotar movemos un poco hacia arriba la partícula de explosión para que coincida
               //con el impacto            
               Explotar();               
           }
        
    }

    private void OnCollisionEnter2D(Collision2D colision)  {
       //Si cae al suelo (choca con escenario) debe explotar el proyectil   
       //y actualizar el valor del impacto si procede
       if (colision.transform.CompareTag("Suelo")) {
               rb.velocity=Vector2.zero;                            
               if (activo) {   
                  Explotar();
               }     
        }
    }


    private void OnTriggerEnter2D(Collider2D collision) {  
       if (collision.CompareTag("Enemigo")) {
            rb.velocity=Vector2.zero;                            
               if (activo) {   
                  Explotar();
               }     
        }            
    }

    private void Explotar()
    {
        Destroy(gameObject);
        Canon.disparo=false;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Clase para controlar el Nivel2:
//-estado: Controla si el nivel a está pausado o aún no empezado (Espera), si estamos jugando (Iniciada) o si se ha terminado las balas o alcanzamos 10 barriles (Finalizada)
//-barriles: Variable pública y estática que puede ser modificada desde otros scripts para el contador de barriles
//-balas: Variable pública y estática que puede ser modificada desde otros scripts para el contador de balas
//-contadorActivo: permite controlar si se activa o no el contador de tiempo.


public class Nivel2 : MonoBehaviour
{
    public static int barriles;
    public static int balas; //actualizar deste 

    public Text marcadorBarriles;
    public Text marcadorBalas;

    public GameObject dialogo;
    public GameObject final;
    public GameObject opciones;
    public GameObject contenedorFade;
    Animator fade;
    GameObject botonNuevoIntento;
    GameObject botonEmpezar;
    GameObject botonContinuar;
    GameObject botonInicio;
    GameObject botonPausar;
    
    public enum Estados {Pausada,Iniciada,Finalizada};
    public static Estados estado;

    private bool contadorActivo;
    Text textoDialogos;

    void Start()
    {
        JuegoControl.control.Musica(false);
        contenedorFade.SetActive(true);
        fade = contenedorFade.GetComponent<Animator>();
        fade.SetBool("FadeOut",false); 
        botonNuevoIntento = opciones.transform.GetChild(0).gameObject;
        botonEmpezar = opciones.transform.GetChild(1).gameObject;
        botonContinuar = opciones.transform.GetChild(2).gameObject;
        botonInicio = opciones.transform.GetChild(3).gameObject;


        string textoNivel="Ha llegado la hora de mostrar tu habilidad con la puntería. Dispones de un máximo de 15 balas para destruir todos los barriles.\n\nApuntar y Fuerza: Puntero del Ratón\nBotón Izquierdo: Disparar\nPausa: Tecla «p»";
        balas=15; 
        barriles=10;
        estado=Estados.Pausada; 

        final.SetActive(false);
        dialogo.SetActive(true);      
        textoDialogos= dialogo.transform.GetChild(0).GetComponent<Text>();
        StartCoroutine(EscribirTexto(textoDialogos, textoNivel,0.05f));
        
        JuegoControl.control.partida.jugando=false;
        JuegoControl.control.partida.nivel=2;                
        Invoke("MostrarEmpezar", 2f);
    }

    void FixedUpdate()
    {
        marcadorBalas.text=balas.ToString();
        marcadorBarriles.text=barriles.ToString();
        

        if (estado==Estados.Iniciada){
            dialogo.SetActive(false);
            EmpezarNivel();
        }

        if (estado==Estados.Finalizada){
            if (JuegoControl.control.partida.jugando) {
                Invoke("ComprobarResultado",2f);
            }
            FinalizarNivel();            
        } 

        if (balas==0 || barriles==0) {
            if (Canon.disparo==false){
                estado=Estados.Finalizada;          
            }
        }
        
        if (Input.GetKey("p")){
            if (estado==Estados.Iniciada){
                estado=Estados.Pausada;
                MostrarPausar();
            }
        }
  
    }

    private void ComprobarResultado()
    {
        if (barriles==0 && balas>=0) {
            final.SetActive(true);
        } 

        if (barriles>0 && balas==0){
            MostrarPerder();
        }
    }

    public void EmpezarNivel(){  
        JuegoControl.control.partida.jugando=true; 
    }


    public void FinalizarNivel(){       
        JuegoControl.control.partida.jugando=false;

    }


    void MostrarEmpezar(){  
        opciones.SetActive(true);
        botonNuevoIntento.SetActive(false);
        botonEmpezar.SetActive(true);
        botonContinuar.SetActive(false);
        botonInicio.SetActive(false);
    }


    void MostrarPausar(){
        JuegoControl.control.partida.jugando=false;        
        opciones.SetActive(true);
        botonNuevoIntento.SetActive(true);
        botonEmpezar.SetActive(false);
        botonContinuar.SetActive(true);
        botonInicio.SetActive(true);
    }

    void MostrarPerder(){
         string textoFinalTiempo="Lo siento Xan, no puedes llevarte la llave, has agotado las balas y no has alcanzado los 10 barriles destruídos. \n Vuelve a intentarlo más tarde.";
         textoDialogos.text="";
         dialogo.SetActive(true);
         StartCoroutine(EscribirTexto(textoDialogos, textoFinalTiempo,0.05f));
         Invoke("MostrarOpciones", 6f);
    }
    
    void MostrarOpciones(){
        botonInicio.SetActive(true);
        botonNuevoIntento.SetActive(true);
    }


   //Mediante esta función simulamos un texto escrito caracter a caracter
    // con una velocidad determinada al llamarse como co-rutina
    // @param1 el componente Text que dónde escribiremos el texto
    // @param2 el mensaje
    // @param3 el tiempo que queremos esperar (x.xf) entre cada carácter
    IEnumerator EscribirTexto(Text texto,string mensaje, float velocidad)
    {
        foreach (char caracter in mensaje){
            texto.text = texto.text + caracter;
            yield return new WaitForSeconds(velocidad); 
        }    
    }


}

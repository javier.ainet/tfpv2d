using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Clase con los métodos empleados por los botones de la UI en la escena Nivel2
public class Nivel2UI : MonoBehaviour
{
    public GameObject opciones;

    public void EmpezarNivel(){
      Nivel2.estado=Nivel2.Estados.Iniciada;  
      gameObject.SetActive(false);
    }

     public void NuevoIntento(){
      SceneManager.LoadScene("Nivel2");
    }

    public void VolverInicio(){
      SceneManager.LoadScene("Nivel0");
    }


    public void ReanudarJuego(){ 
      Nivel2.estado=Nivel2.Estados.Iniciada; 
      JuegoControl.control.partida.jugando=true; 
      opciones.transform.GetChild(0).gameObject.SetActive(false);
      opciones.transform.GetChild(2).gameObject.SetActive(false);
      opciones.transform.GetChild(3).gameObject.SetActive(false);     
    }
}

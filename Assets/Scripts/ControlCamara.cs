using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase para el control de la cámara, conlleva añadir un objetivo para que la siga
//Ese objetivo es el jugador, y su funcionamiento es transformar la posición de la cámara 
//Aplica un factor de suavizado cuando el jugador cambia de posición.

public class ControlCamara : MonoBehaviour
{
    public Transform objetivo;
    public Vector3 desplazamiento;

    [Range(1,10)]
    public float factorSuavizado;
    
    private void FixedUpdate() {
    {
      Seguir();
    }

    void Seguir() {
          Vector3 destino = objetivo.position + desplazamiento;
          Vector3 movimientoSuavizado = Vector3.Lerp(transform.position, destino, factorSuavizado*Time.deltaTime);
          transform.position = movimientoSuavizado;
      }
    } 
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase para controlar los movimientos del jugador peronaje:
//- Información del personaje: velocidad, salto
//- Controlamos los movimientos izquierda-derecha (teclas dirección)
//- Controlamos los saltos (tecla espacio) y amplitud del mismo
//- Controlamos disparo hachas nivel3
//- Controlamos si colisiona enemigo y pierde partida
public class ControlJugador : MonoBehaviour
{
    public float velocidad=2;
    public float salto=5f;
    public float saltoAlto=2f;
    public float saltoBajo=0.1f; 

    public GameObject hacha;

    public static bool muerto;
    

    Rigidbody2D rb;
    SpriteRenderer sr;
    Animator anim;
 
    private bool activo;
    
    private bool lanzar;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        anim.SetBool("Vivo", true);
        activo=false;
        lanzar = JuegoControl.control.partida.lanzar;
    }

    void FixedUpdate()   
    {

        lanzar = JuegoControl.control.partida.lanzar;
        if (!JuegoControl.control.partida.jugando){
            activo=false;
            anim.SetBool("Correr", false);
            rb.velocity=new Vector2(0,rb.velocity.y);
        }


        if (JuegoControl.control.partida.jugando){
            activo=true;
        }


        if (activo){
            //Controlamos desplazamiento hacia la derecha       
            if (Input.GetKey("right"))                
            {
                sr.flipX= false;
                anim.SetBool("Correr", true);
                rb.velocity=new Vector2(velocidad,rb.velocity.y);            
            }
            //Controlamos desplazamiento hacia la izquierda      
            else if (Input.GetKey("left"))
            {
                sr.flipX= true;
                anim.SetBool("Correr", true);
                rb.velocity=new Vector2(-velocidad,rb.velocity.y);              
            }
            //Si no se mueve activamos pausa
            else
            {
                anim.SetBool("Correr", false);
                rb.velocity=new Vector2(0,rb.velocity.y);
            }

            //Controlamos que se pueda saltar y ejecutamos salto.
            if (Input.GetKey("space") && ControlSuelo.suelo)
            {            
                rb.velocity=new Vector2(rb.velocity.x,salto);            
                //audio salto
                AudioSource audio = gameObject.transform.GetChild(0).gameObject.GetComponent<AudioSource>();
                audio.PlayOneShot(audio.clip);   
            }


            //Controlamos el disparo sólo nivel 3
            if (Input.GetKey("x") && lanzar==false)
            {            
                if (JuegoControl.control.partida.nivel==3 && Nivel3.hachas>0){
                    anim.SetTrigger("Atacar");                    
                    Vector3 pos = new Vector3(transform.position.x,transform.position.y+0.25f,transform.position.z);                    
                    GameObject hachaInstancia = Instantiate(hacha,pos,transform.rotation); 
                    hachaInstancia.transform.GetChild(0).GetComponent<SpriteRenderer>().flipX= sr.flipX;
                    JuegoControl.control.partida.lanzar=true; 
                }
               
            }
            
            //Controlamos la animación del salto
            if (ControlSuelo.suelo==false)      
            {
                anim.SetBool("Saltar", true);
            } else
            {
                anim.SetBool("Saltar", false);
            }
            
            //Con este código controlamos el salto en función de cuanto presionamos tecla
            //consiguiendo que salte poco o mucho el jugador
            if (rb.velocity.y<0)
            {
                rb.velocity += Vector2.up*Physics2D.gravity * (saltoBajo) * Time.deltaTime;
            } 
            else if (rb.velocity.y>0 && !Input.GetKey("space"))
            {
                rb.velocity += Vector2.up*Physics2D.gravity * (saltoAlto) * Time.deltaTime;
            }

        }
    } 

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.transform.CompareTag("Enemigo")) {
           muerto=true;
           anim.SetBool("Morir",true);
           AudioSource audio = GetComponent<AudioSource>();
           audio.PlayOneShot(audio.clip);  
           JuegoControl.control.partida.jugando=false;                    
        }    
        
    }

}

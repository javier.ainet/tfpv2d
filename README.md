# UOC - PROGRAMACIÓN VIDEOJUEGOS 2D - PRÁCTICA FINAL
Práctica Final de la asignatura Programación de Videojuegos en 2D (2022) 

# RESUMEN
«Las llaves D'Arzúa» es un juego de plataformas y habilidades en el que se han intentado poner en práctica de manera conjunta todo lo trabajado y aprendido en las prácticas anteriores, consiguiendo una experiencia de juego completa. La historia se basa en encarnar el papel de un personaje medieval que debe pasar 3 pruebas (niveles) para conseguir en cada uno 1 llave que le permita acceder a la logia D'Arzúa.

# VÍDEO EXPLICATIVO FUNCIONAMIENTO

https://www.youtube.com/watch?v=_Nqc5TPQUTI

# INSTRUCCIONES

## OBJETIVO
La finalidad del juego es que encarnando el papel del protagonista (XAN) consigamos las 3 llaves que permiten acceder al templo D'Arzúa y pertenecer a ese grupo. Para conseguir cada llave debemos superar su correspondiente prueba, indistintamente del orden: Velocidad, Puntería y Valentía.

Los intentos son ilimitados una vez comenzada la partida.

## NIVELES DEL JUEGO
### Nivel 1 - Velocidad
En esta prueba tienes un tiempo de 90s para recoger 25 calabazas, es obligatorio recogerlas y llegar antes del final del tiempo a la estatua D'Arzúa.

Controles para este nivel:
* Moverse Izq-Dcha: Teclas de dirección
* Saltar: Barra espaciadora 
* Pausar: Tecla "p"

### Nivel 2 - Puntería
Para conseguir la llave en esta prueba es necesario derribar al menos 10 barriles, y para ello dispones de 15 balas en tu cañón.

Controles para este nivel:
* Apuntar: Puntero ratón
* Fuerza: Distancia del puntero ratón al cañón
* Disparar: Click botón primario del ratón
* Pausar: Tecla "p"

### Nivel 3 - Valentía
En este nivel entrarás en una cueva oscura llena de almas errantes, debes evitar que te toquen o morirás.
Para acabar con las almas errantes dispones de hachas mágicas que puedes lanzarles, pero tienes un número limitado de ellas, así que deberás emplearlas con cuidado. Sobrevive a las almas y llega a la estatua D'Arzúa.

Controles para este nivel:
* Moverse Izq-Dcha: Teclas de dirección
* Saltar: Barra espaciadora 
* Disparar: Tecla "x"
* Pausar: Tecla "p"

## EMPEZAR UNA PARTIDA
En la pantalla MENÚ PRINCIPAL  se debe elegir entre alguno de los 3 modos de juego, tras la selección nos lleva a la pantalla del JUEGO.


# DESARROLLO E IMPLEMENTACIÓN UNITY

## Organización Escenas

El desarrollo consta de 10 escenas:

01 Logo
Esta pantalla de inicio del juego muestra los logos del estudio (ficticio) de desarrollo y el logo del juego.

02 Intro
Esta pantalla nos introduce en la historia del juego, en ella un Mago nos cuenta en qué consiste. Se puede omitir mediante el botón correspondiente.

03 Menú
Esta pantalla es el menú principal del juego donde contamos con diferentes opciones:
- Nueva Partida: Comenzar una partida desde 0, sin ninguna llave.
- Continuar Partida : Continuar una partida iniciada.
- Instruciones: Acceso a escena/pantalla con resumen teclas e instrucciones niveles.
- Ver Intro: Volver a ver la intro del juego.
- Salir: Salir de la aplicación.
- Créditos: Información sobre la autoría y recursos empleados realizar el juego.

04 Nivel0
Esta pantalla es en la que se muestra el progreso del juego (las llaves que llevamos/faltan) y dónde el Mago nos va dando explicaciones, y también desde dónde elegiremos que nivel queremos jugar. Al finalizar cada nivel o cuando lo deseemos podremos volver a ella.

05 Nivel1
Esta es la primera pantalla de juego de la prueba de VELOCIDAD. Se ha implementando empleando el componente Grid y TileMap. Además del control del personaja ese han empleado scripts para recoger las calabazas y métodos para realizar los contadores como el del tiempo.

06 Nivel2
Esta es la primera pantalla de juego de la prueba de PUNTERÍA. Los scripts más importantes y los componentes es el prefab de la bala y el movimiento del cañón estableciendo la dirección y fuerza adecuadas.

07 Nivel3
Esta es la primera pantalla de juego de la prueba de VALENTÍA.Se ha implementando empleando el componente Grid y TileMap, además del componente de Unity - Universal Render Pipeline, que ha permitido gracias a sus componentes implementar las luces y y la simulación de oscuridad del juego y la iluminación del escenario y materiales. Este componente también se ha empleado en la intro de los logos.

08 Final
Pantalla final que muestra al personaje entrando en castillo, creado mediante animación del personaje y movimiento.

09 Instrucciones
Pantalla con sprites mostrando información de instrucción teclas/reatón de juego para cada nivel.

10 Créditos
Pantalla con scroll de texto mostrando información relativa a autor y recursos empleados juego.



## Diseño de niveles
Todos los niveles se han confeccionando mediante GRID y TILEMAP en base a paletas del escenario definidas en 16x16 pixeles.

## Estructuras de Datos (ED)
A continuación se explican las clases más importantes que se han creado y su finalidad en cada parte del juego.

PARTIDA
Contiene los datos necesarios para gestionar estados de la misma:
- estado: para conocer los estados de la partida (Pausada, Iniciada o Finalizada).
- llaves: array de 3 elementos booleanos para contar las llaves ya encontradas o pendientes.
- nivel: conocer el nivel del juego que se está jugando en un momento dado.
- jugando: conocer si el jugador está jugando(true) o en pausa (false).
- lanzar: determina si está lanzando algo (true) o pendiente de lanzar (false), esto permite ir disparando proyectiles en los niveles de 1 en 1.

Tiene además los métodos necesarios para su construcción y diferentes funcionescomo conocer el número de llaves o cambiar estados de la partida.

CONTROLJUGADOR 
Esta clase se emplea en los niveles 1 y 3, permite reaccionar a los controles pulsados aplicando los movimientos (cambio de posición en escena) y la animación del personaje de acuerdo con ellos, así como la animación de lanzar objeto.

CONTROLENEMIGO
Esta clase establece el movimiento automático-aleatorio de los enemigos del nivel 3, y controla si se destruyen al recibir un impacto de hacha o si elimina al jugador.


CLASES EN CADA NIVEL PARA CONTROL Y UI
En cada nivel se ha creado una clase concreta para gestionar el juego en el mismo, alguna clase específica para controlar los elementos que interactuan nivel como RECOLECTAR, CANON, BARRIL, BALA, HACHA o CONTROLENEMIGO, y también, clase en cada nivel para controlar el UI gestionando las pausas y los botones de acción de la misma.



## Control del juego entre escenas
Se ha creado un objeto JuegoControl (clase singleton) que se autoinstancia y no se destruye entre escenas para guardar la información de la partida.


# MEJORAS
1. Se puede optimizar mejor los scripts que gestionan la UI en los distintos niveles, ya que las funciones son similares.
2. Se podría aumentar estados por nivel en la propia clase PARTIDA y optimizar/reducir código en niveles.



# CRÉDITOS
*AUTOR: Fco. Javier Feijoo Lopez  (Licencia GNU GPL)

*ENTREGA: PRÁCTICA FINAL Videojuegos 2D 2022

*HERRAMIENTAS: Unity 3D v2020.3.0f1

*TIPOGRAFIAS  TTF:  Vintage Avalanche y Ancient Medium -  www.dafont.com

*IMAGENES Y TEXTURA FONDOS/ESCENARIOS:
 - Diseños propios y tomados adaptados de Medieval Warrior Pack (Luizmelo) https://itch.io/ 
 - Diseños adaptados y tomados de Pixel Art Platformer - Village Props (Cainos) de Asset Store Unity
 - Diseños adaptados y tomados de 2D Platfrom Tile Set - Cave(Iphigenia Pixels) de Asset Store Unity
 - Diseños adaptados y tomados de freepik.es

*EXPLOSIONES - EFECTOS VISUALES:
 - Diseños adaptados y tomados Pyro Particles (Jeff Johnson) de Asset Store Unity
 - Universal Reder Pileline de Asset Store Unity

*AUDIOS:
- www.freesound.org bajo licencia CC Attribution 3.0 
- www.bensound.com
	


